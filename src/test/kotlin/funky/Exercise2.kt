package funky

import io.kotlintest.properties.forAll
import io.kotlintest.specs.StringSpec

/**
 * @author Pablo Barrientos (pbarrientos@console.com.au)
 */
class Exercise2: StringSpec() {
    init {
        "curry works" {
            forAll { s: String, n: Int ->
                val curriedSubSequence = s::subSequence.curry()
                val takeFromMyString = curriedSubSequence(0)
                n < 0 || takeFromMyString(Math.min(n, s.length)) == s.take(n)
            }
        }

        "uncurry works" {
            forAll { a: Int, b: Int ->
                val plusInt: ((Int) -> ((Int) -> Int)) = { a -> { b -> a + b } }
                plusInt.uncurry()(a, b) == a + b
            }
        }

    }
}