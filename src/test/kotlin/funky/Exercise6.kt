package funky

import io.kotlintest.properties.forAll
import io.kotlintest.specs.StringSpec

/**
 * @author Pablo Barrientos (pbarrientos@console.com.au)
 */
class Exercise6: StringSpec() {
    init {
        "List.filterWithFlatMap works" {
            forAll { list: List<Int> ->
                fun isOdd(x: Int) = x % 2 != 0
                list.filterWithFlatMap(::isOdd) == list.filter(::isOdd)
            }
        }

        "List.filterWithFold works" {
            forAll { list: List<Int> ->
                fun isOdd(x: Int) = x % 2 != 0
                list.filterWithFold(::isOdd) == list.filter(::isOdd)
            }
        }

    }
}