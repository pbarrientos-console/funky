package funky

import io.kotlintest.properties.forAll
import io.kotlintest.specs.StringSpec

/**
 * @author Pablo Barrientos (pbarrientos@console.com.au)
 */
class Exercise5: StringSpec() {
    init {
        "List.mapWithFoldRight works" {
            forAll { list: List<Int> ->
                val f: (Any) -> Int = constant(1)
                list.mapWithFoldRight(f)  == list.map(f)
            }
        }


    }
}