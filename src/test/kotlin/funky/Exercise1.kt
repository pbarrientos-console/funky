package funky

import io.kotlintest.properties.forAll
import io.kotlintest.specs.StringSpec

/**
 * @author Pablo Barrientos (pbarrientos@console.com.au)
 */
class Exercise1: StringSpec() {
    init {
        "constant works" {
            forAll { a: Int, b: String ->
                constant<Any, Any>(a)(b) == a
            }
        }

        "flip works" {
            forAll { a: Int, b: Int ->
                val plusInt: (Int, Int) -> Int = { x, y -> x + y }
                plusInt.flip()(a, b) == b + a
            }
        }

        "andThen works" {
            forAll { a: String, b: String, c: String ->
                (a::plus.andThen { it + c })(b) == a + b + c
            }
        }
    }
}