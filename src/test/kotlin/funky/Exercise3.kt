package funky

import io.kotlintest.properties.Gen
import io.kotlintest.properties.forAll
import io.kotlintest.specs.StringSpec

/**
 * @author Pablo Barrientos (pbarrientos@console.com.au)
 */
class Exercise3: StringSpec() {
    init {
        "BinaryTree<Int>.maximum works" {
            forAll(BinaryTreeIntGen) { bt: BinaryTree<Int> ->
                val max = bt.maximum()
                bt.fold( { it <= max }, Boolean::and)
            }
        }

        "BinaryTree.mapWithFold works" {
            forAll(BinaryTreeIntGen) { bt: BinaryTree<Int> ->
                val f: (Any) -> Int = constant(0)
                bt.mapWithFold(f) == bt.map(f)
            }
        }
    }
}

object BinaryTreeIntGen: Gen<BinaryTree<Int>>{
    override fun generate(): BinaryTree<Int> {
        val generateLeaf = Gen.bool().generate()
        return if (generateLeaf) {
            val v = Gen.int().generate()
            Leaf(v)
        } else {
            val left = this.generate()
            val right = this.generate()
            Branch(left, right)
        }
    }

}