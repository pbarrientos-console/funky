package funky

import io.kotlintest.properties.forAll
import io.kotlintest.specs.StringSpec

/**
 * @author Pablo Barrientos (pbarrientos@console.com.au)
 */
class Exercise4: StringSpec() {
    init {
        "List.lenghtWithFold works" {
            forAll { list: List<Int> ->
                list.lenghtWithFold() == list.size
            }
        }


    }
}