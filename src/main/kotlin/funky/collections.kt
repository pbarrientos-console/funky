package funky

/**
 * @author Pablo Barrientos (pbarrientos@console.com.au)
 */
val collection = 1 .. 10

val until5 = collection.takeWhile { it < 6 }

val after5 = collection.dropWhile { it < 6 }

val group = collection.groupBy { it % 2 }

val foldListInt = listOf(1, 2, 3, 4).fold(0 , { acc, i -> acc + i})
val foldListString = listOf("funKy", "Kotlin").fold("" , { acc, i -> acc + i})

val foldLPlus = listOf(1, 2, 3).fold(0, { acc, i -> acc + i })
val foldRPlus = listOf(1, 2, 3).foldRight(0, { acc, i -> acc + i })

val foldLMinus = listOf(1, 2, 3).fold(0, { acc, i -> acc - i })
val foldRMinus = listOf(1, 2, 3).foldRight(0, { acc, i -> acc - i })

// Exercise 4
fun <A> List<A>.lenghtWithFold(): Int = TODO()

// Exercise 5
fun <A, B> List<A>.mapWithFoldRight(f:(A) -> B): List<B> = TODO()


fun neighbours(i: Int) = listOf(i-1, i, i+1)
val n = listOf(-10, 0, 10).flatMap( ::neighbours )

// Exercise 6a
fun <A> List<A>.filterWithFlatMap(p:(A) -> Boolean): List<A> = TODO()

// Exercise 6b
fun <A> List<A>.filterWithFold(p:(A) -> Boolean): List<A> = TODO()
