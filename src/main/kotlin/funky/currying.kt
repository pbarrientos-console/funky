package funky

/**
 * @author Pablo Barrientos (pbarrientos@console.com.au)
 */

//exercise 2
object Curry {
    fun <A, B, C> curry(f: (A, B) -> C): (A) -> ((B) -> C) = TODO()

    fun <A, B, C> uncurry(f: (A) -> ((B) -> C)): (A, B) -> C = TODO()
}


fun <A,B,C> ((A, B) -> C).curry():((A) -> ((B) -> C)) = Curry.curry(this)

fun <A,B,C> ((A) -> ((B) -> C)).uncurry(): ((A, B) -> C) = Curry.uncurry(this)


fun example(){
    val curriedSubSequence = "myString"::subSequence.curry()
    val takeFromMyString = curriedSubSequence(0) //subseq starting at 0
    val firstTwo = takeFromMyString(2)
}

fun <A,B,C, D> ((A, B, C) -> D).curry():((A) -> ((B) -> ((C) -> D))) = TODO()

