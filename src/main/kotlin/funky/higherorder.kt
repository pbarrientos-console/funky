package funky

/**
 * @author Pablo Barrientos (pbarrientos@console.com.au)
 */
infix fun <A, B, C> ((B) -> C).compose(f: (A) -> B): (A) -> C = { a: A -> this(f(a)) }

fun example1() {
    fun sqr(x: Double) = Math.pow(x, 2.0)
    fun fourthPower(x:Double) = ::sqr.compose(::sqr)(x)
    fourthPower(2.0)
}

fun rewriteAsVal(){
    val sqr = { x: Double -> Math.pow(x, 2.0) }
    val fourthPower = { x: Double -> sqr.compose(sqr)(x) }
    fourthPower(2.0)
}

fun rewriteEtha(){
    val sqr = { x: Double -> Math.pow(x, 2.0) }
    val fourthPower = sqr.compose(sqr)
    fourthPower(2.0)
}

fun example2(){
    fun isOdd(x: Int) = x % 2 != 0
    val oddLength = ::isOdd.compose(String::length)
    val strings = listOf("a", "ab", "abc")
    println(strings.filter(oddLength)) // Prints "[a, abc]"
}