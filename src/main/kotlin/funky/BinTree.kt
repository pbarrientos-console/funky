package funky

/**
 * @author Pablo Barrientos (pbarrientos@console.com.au)
 */
sealed class BinaryTree<out A> {
    fun <B> fold(f: (A) -> B, g: (B, B) -> B): B =
            when (this) {
                is Leaf -> f(this.value)
                is Branch -> {
                    val resultFromLeft = this.left.fold(f, g)
                    val resultFromRight = this.right.fold(f, g)
                    g(resultFromLeft, resultFromRight)
                }
            }

    fun <B> map(f: (A) -> B): BinaryTree<B> =
            when (this) {
                is Leaf -> Leaf(f(this.value))
                is Branch -> Branch(this.left.map(f), this.right.map(f))
            }

    fun size(): Int =
            this.fold({_ -> 1},
                    { resultFromLeft, resultFromRight ->
                        1 + resultFromLeft + resultFromRight } )

    fun depth(): Int = fold( {_ -> 0}, { d1,d2 -> 1 + Math.max(d1, d2) } )
}

data class Leaf<A>(val value: A): BinaryTree<A>()
data class Branch<A>(val left: BinaryTree<A>, val right: BinaryTree<A>): BinaryTree<A>()

// Exercise 3a
fun BinaryTree<Int>.maximum():Int = TODO()

fun <A, B> BinaryTree<A>.mapWithFold(f: (A) -> B):BinaryTree<B> = TODO()