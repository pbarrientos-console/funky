package funky

/**
 * @author Pablo Barrientos (pbarrientos@console.com.au)
 */
fun <A> identity(x:A) = x

// exercise 1
fun <P, T> constant(t: T): (P) -> T = TODO()
fun <P1, P2, R> ((P1, P2) -> R).flip(): (P2, P1) -> R = TODO()
fun <P, Q, R> ((P) -> Q).andThen(f: (Q) -> R): (P) -> R = TODO()